const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const History = require('./models/History');
const User = require('./models/User');
mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('artists');
        await db.dropCollection('tracks');
        await db.dropCollection('albums');
        await db.dropCollection('histories');
        await db.dropCollection('users');
    }
    catch (e) {
        null
    }


    const [rammstein, kabzon] = await Artist.create({
        name: 'Rammstein',
        about: 'Great classic',
        image: 'ram.jpeg',
        published:true
    }, {
        name: 'Iosif Kabzon',
        about: 'Super Artist',
        image: 'kbz.jpg'
    });

    const [mutt, ross, best] = await Album.create({
        name: 'Mutter',
        author: rammstein._id,
        year: 2001,
        image: 'mutt.jpg',
        published:true
    }, {
        name: 'Rosenrot',
        author: rammstein._id,
        year: 2005,
        image: 'ros.jpg'
    }, {
        name: 'Легендарные песни',
        author: kabzon._id,
        year: 2013,
        image: 'best.jpg'
    },);
    await Track.create({
            name: 'Du hast',
            length: '02:01:10',
            album: mutt._id,
            num: 1,
            published:true
        },
        {
            name: 'Mutter',
            length: '02:03:10',
            album: mutt._id,
            num: 3
        },
        {
            name: 'Zwitter',
            length: '03:01:10',
            album: mutt._id,
            num: 4
        },
        {
            name: 'Adios',
            length: '02:01:10',
            album: mutt._id,
            num: 5
        },
        {
            name: 'Ich will',
            length: '03:11:10',
            album: mutt._id,
            num: 2
        },
        {
            name: 'Mann geggen Mann',
            length: '03:01:00',
            album: ross._id,
            num: 1
        },
        {
            name: 'Zerstören',
            length: '02:01:00',
            album: ross._id,
            num: 2
        },
        {
            name: 'Wo bist du?',
            length: '03:41:00',
            album: ross._id,
            num: 3
        },
        {
            name: 'Hilf mir',
            length: '03:23:00',
            album: ross._id,
            num: 4
        },
        {
            name: 'Benzin',
            length: '03:44:00',
            album: ross._id,
            num: 5
        },
        {
            name: 'Журавли',
            length: '03:01:00',
            album: best._id,
            num: 1
        },
        {
            name: 'Погоня',
            length: '01:21:30',
            album: best._id,
            num: 2
        },
        {
            name: 'Белое солнце',
            length: '03:31:00',
            album: best._id,
            num: 3
        },
        {
            name: 'Звёзды на небе',
            length: '02:29:30',
            album: best._id,
            num: 4
        },
        {
            name: 'Мне доверена песня',
            length: '02:21:00',
            album: best._id,
            num: 5
        },
        {
            name: 'Всё повторяется',
            length: '03:21:30',
            album: best._id,
            num: 6
        });

    await User.create({
            name: 'user',
            password: '123',
            role: 'user'
        },
        {
            name: 'admin',
            password: 'admin123',
            role: 'admin'
        });


    db.close();
});