const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const config = require('./config');

const track = require('./app/track');
const artist = require('./app/artist');
const albums = require('./app/album');
const users = require('./app/users');
const history = require('./app/history');

const app = express();
const port = 8000;


app.use(express.json());
app.use(cors());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;


db.once('open', () => {
    console.log('Mongoose connect');

    app.use('/tracks', track());
    app.use('/artists', artist());
    app.use('/albums', albums());
    app.use('/users', users());
    app.use('/track_history',history());

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});