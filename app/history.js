const express = require('express');
const Artist = require('../models/Artist');
const User = require('../models/User');
const History = require('../models/History');

const router = express.Router();

const createRouter = () => {


    router.get('/', (req, res) => {
        const token = req.get('Token');
        User.findOne({token})
            .then(user => {
                if (!user){
                    res.sendStatus(401);}
                else {
                    History.find({user:user._id}).sort({date:-1})
                        .populate({path: 'track_id',
                            populate: {path: 'album',
                                populate: {path: 'author', select: 'name'}}})
                        .then(list => res.send(list))
                        .catch(err => res.status(400).send(err));
                }
            },error=>res.sendStatus(400).send("ERROR"));
    });

    router.post('/', (req, res) => {
        const token = req.get('Token');

        User.findOne({token: token})
            .then(user => {
                if (user.length === 0)
                    res.sendStatus(401);
                else {
                    const history = new History(req.body);
                    history.user = user._id;
                    history.date = new Date().toISOString();
                    history.save().then(hist => res.send(hist)).catch(() => res.status(400).send('Error'));

                }
            })
            .catch(error => res.sendStatus(500));

    });

    return router;
};

module.exports = createRouter;