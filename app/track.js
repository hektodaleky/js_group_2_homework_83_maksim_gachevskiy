const express = require('express');
const Track = require('../models/Track');

const router = express.Router();
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const createRouter = () => {
    /*Есть возможность вывожить все песни автора*/

    router.get('/', (req, res) => {
        Track.find({published:true}).sort({num: 1}).populate({path: 'album', populate: {path: 'author', select: 'name'}})
            .then(track => req.query.album ?
                res.send(track.filter(item => item.album.name.toLowerCase() === req.query.album.toLowerCase()))
                : (req.query.artist ?
                    res.send(track.filter(item => item.album.author.name.toLowerCase() === req.query.artist.toLowerCase()))
                    : res.send(track)))
            .catch(() => res.sendStatus(500))
    });

    router.get('/admin',[auth,permit('admin')], (req, res) => {

        Track.find()
            .then(track => res.send(track))
            .catch(error => res.sendStatus(500));
    });
    router.post('/', (req, res) => {
        const track = new Track(req.body);
        Track.find({album: track.album}).then(response => {

            track.num = response.length + 1;
            track.save()
                .then(track => res.send(track))
                .catch(error => res.status(400).send(error));
        })

    });
    router.post('/approve', async (req, res) => {

        const item = await Track.findOne({_id: req.body.id});


        if (!item) {
            return res.status(400).send({error: 'Track not found'});

        }



        item.published = true;
        item.save().then(track => res.send(track))
            .catch(error => res.status(400).send(error))

    });


    router.delete('/:id', [auth, permit('admin')], (req, res) => {
        Track.deleteOne({_id: req.params.id})
            .then(response => res.send('Success'))
            .catch(err => res.send(err))
    });
    return router;
};

module.exports = createRouter;