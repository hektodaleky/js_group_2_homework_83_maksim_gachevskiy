const express = require('express');
const Album = require('../models/Album');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    }, filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});


const upload = multer({storage});

const createRouter = () => {
    router.get('/', (req, res) => {
        Album.find({published: true}).populate('author')
            .then(albums => req.query.artist ?
                res.send(albums.filter(item => item.author.name.toLowerCase() === req.query.artist.toLowerCase()))
                : res.send(albums))
            .catch(error => {
                console.log(error);
                res.sendStatus(500)
            });
    });
    router.get('/admin', [auth, permit('admin')], (req, res) => {

        Album.find()
            .then(album => res.send(album))
            .catch(error => res.sendStatus(500));
    });

    router.post('/', upload.single('image'), (req, res) => {
        const album = new Album(req.body);
        if (req.file) {
            album.image = req.file.filename;
        }


        album.save()
            .then(album => res.send(album))
            .catch(err => res.status(400).send(err));
    });

    router.post('/approve', async (req, res) => {

        const item = await Album.findOne({_id: req.body.id});


        if (!item) {
            return res.status(400).send({error: 'Album not found'});

        }


        item.published = true;
        item.save().then(album => res.send(album))
            .catch(error => res.status(400).send(error))

    });

    router.delete('/:id', [auth, permit('admin')], (req, res) => {

        console.log(req.params.id);
        Album.deleteOne({_id: req.params.id})
            .then(response => res.send('Success'))
            .catch(err => res.send(err))
    });
    return router;
};

module.exports = createRouter;