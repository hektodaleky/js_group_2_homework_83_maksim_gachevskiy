const express = require('express');
const Artist = require('../models/Artist');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    }, filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});


const upload = multer({storage});

const createRouter = () => {
    router.get('/', (req, res) => {

        Artist.find({published: true})
            .then(artist => res.send(artist))
            .catch(error => res.sendStatus(500));
    });

    router.get('/admin', [auth, permit('admin')], (req, res) => {

        Artist.find()
            .then(artist => res.send(artist))
            .catch(error => res.sendStatus(500));
    });

    router.post('/', upload.single('image'), (req, res) => {
        const artist = new Artist(req.body);
        if (req.file) {
            artist.image = req.file.filename;
        }

        console.log(artist);
        artist.save()
            .then(artist => res.send(artist))
            .catch(err => res.status(400).send(err));
    });

    router.post('/approve', async (req, res) => {

        const item = await Artist.findOne({_id: req.body.id});


        if (!item) {
            return res.status(400).send({error: 'Artist not found'});

        }


        item.published = true;
        item.save().then(artist => res.send(artist))
            .catch(error => res.status(400).send(error))

    });



    router.delete('/:id', [auth, permit('admin')], (req, res) => {

        console.log(req.params.id);
        Artist.deleteOne({_id: req.params.id})
            .then(response => res.send('Success'))
            .catch(err => res.send(err))
    });


    return router;
};

module.exports = createRouter;