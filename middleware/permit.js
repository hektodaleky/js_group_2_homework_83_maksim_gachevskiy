const permit = (...roles) => {
    return (req, res, next) => {
        if (!req.user)
            res.sendStatus(401).send({message: 'Unauthenticate'});
        if (!roles.includes(req.user.role)) {
            return res.sendStatus(403).send({message: 'You shall not pass'});
        }
        next();
    }
};

module.exports = permit;