const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ArtistSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    about: String,

    image: String,
    published: { type: Boolean, default: false }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;