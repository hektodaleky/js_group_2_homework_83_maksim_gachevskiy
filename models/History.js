const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HistorySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: 'true'
    },
    track_id: {
        type: Schema.Types.ObjectId,
        ref: 'Track',
        required: 'true'
    },
    date: String
});

const History = mongoose.model('History', HistorySchema);
module.exports = History;