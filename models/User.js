const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;
const SALT_WORK_FACTOR = 10;
const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function (value) {
                if (!this.isModified('name')) return true;
                const user = await User.findOne({name: value});
                if (user) throw new Error('This user already exist');
                return true;
            },
            message: 'This username already exist'
        }
    },
    password: {
        type: String,
        required: true
    },
    token: String,
    role: {
        type: String,
        default: 'user',
        enum: ['admin', 'user']
    }
});

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(this.password, salt);

    this.password = hash;

    next();
});

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password);
};

UserSchema.set('toJSON', {
    transform: (doc, ret, option) => {
        delete ret.password;
        return ret;
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;